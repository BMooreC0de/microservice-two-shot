import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import ShoesList from './ShoesList';
import React, {useEffect, useState} from 'react'


function App(props) {
  const [shoes, SetShoes] = useState([]);
  const [hats, setHats] = useState([]);

  const getShoes = async () => {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const shoes = data.shoes
      SetShoes(shoes)
    }
  }


  const fetchHats = async () => {
  const url = 'http://localhost:8090/api/hats/';
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    setHats(data)
    }

  }

  useEffect(() => {
      fetchHats();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoeList shoes={shoes} />} />
          <Route path="hats" >
            <Route path="" element={<HatsList hats={hats}/>} />
            <Route path="new" element={<HatsForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
