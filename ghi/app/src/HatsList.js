import React, {useEffect, useState} from 'react'


function HatsList(props) {

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Location</th>
                    <th>Remove</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                return (
                    <tr key={hat.href}>
                        <td>{ hat.fabric }</td>
                        <td>{ hat.style_name }</td>
                        <td>{ hat.color }</td>
                        <td>{ hat.location }</td>
                        <td><button type="button" className="btn btn-danger" onClick={() => deleteItem(hat)}>Delete!</button> </td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
    );
  }
export default HatsList;


async function deleteItem(hat) {
    const hatUrl = `http://localhost:8090${hat.href}`;
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    };
   await fetch(hatUrl, fetchOptions);
   window.location.reload(true);
  }
